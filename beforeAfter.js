var containerDivs = document.getElementsByClassName("ycBeforeAfter");
var dividerSize = 10
var dividerColor = "#505050"
var dividerXPos=200

for (var i = 0; i < containerDivs.length; i++) {
	
	beforeAfter(containerDivs[i]);
}

document.onmouseup = function (e) {
	for(i=0;i<containerDivs.length;i++){
		containerDivs[i].onmousemove = null
		containerDivs[i].onmouseup = null
	}
}

function createDivider(imgHeight){
	var divider = document.createElement("div");
	divider.classList.add("ycBeforeAfterDivider");
	divider.style.position = "absolute";
	divider.style.top=0;
	divider.style.zIndex = 10
	divider.style.width = dividerSize + "px"
	divider.style.height = imgHeight + "px"
 	divider.style.backgroundColor = dividerColor
	return divider;
}


function beforeAfter(containerDiv){
	const pics = containerDiv.querySelectorAll("img");
	var beforePic = pics[0];
	var afterPic = pics[1];
	var picWidth = beforePic.width
	var picHeight = beforePic.height
	//Clear the container completely first
	while(containerDiv.hasChildNodes())
	{
		containerDiv.removeChild(containerDiv.lastChild);
	}

	// Re-add the before and after pics
	// Only have the after Pics position relative. We place the beforePic on top. 
	// If both pics would have an absolute size the flow of the page would get messed up. 
	afterPic.style.position = "relative";
	afterPic.zIndex = 1
	afterPic.classList.add("afterPic")
	containerDiv.appendChild(afterPic);

	beforePic.style.position = "absolute";
	beforePic.style.objectFit ="cover"
	beforePic.style.objectPosition="left top"
	beforePic.style.top = 0;
	beforePic.style.left = 0;
	beforePic.height = beforePic.height;
	beforePic.width = 0;
	beforePic.zIndex = 2
	beforePic.classList.add("beforePic");
	containerDiv.appendChild(beforePic);

	var divider = createDivider(beforePic.height);
	containerDiv.appendChild(divider);

	containerDiv.onmousedown = onContainerMouseDown;
	containerDiv.addEventListener('touchstart', onTouchStart, false);
}

function setDividerPos(container,newX){
	var divider = container.querySelector(".ycBeforeAfterDivider");
	dividerXPos = newX;
	if(dividerXPos > container.clientWidth){
		dividerXPos = container.clientWidth;
	}
	if(dividerXPos < 0)
	{
		dividerXPos = 0;
	}
	divider.style.left = dividerXPos + "px";
	beforePic = container.querySelector(".beforePic");
	beforePic.width = dividerXPos;
}

function onContainerMouseDown(e){
	if(e.buttons != 1){
		// Ignore non-left mouse button clicks
		return
	}
	var container = e.target.parentNode
	e.preventDefault()
	container.onmousemove = onDividerDrag
	container.onmouseup = onStopDragging
	var newDividerX = e.pageX - container.offsetLeft - Math.round((dividerSize /2))
	setDividerPos(container,newDividerX);
}

function onDividerDrag(e){
	var container = e.target.parentNode
	//We don't want any default browser image drag behavior
	e.preventDefault()
	var newDividerX = e.pageX - container.offsetLeft - Math.round((dividerSize /2))
	setDividerPos(container,newDividerX);
}

function onStopDragging(e){
	e.target.parentNode.onmousemove = null
	e.target.parentNode.onmouseup = null
}

function onTouchStart(e){
	if(e.targetTouches.length != 1){
		// Only interpret single finger touches
		return
	}
	var container = e.targetTouches[0].target.parentNode;
	container.addEventListener('touchmove', onTouchMove, false);
	container.addEventListener('touchend', onTouchEnd, false);
	container.addEventListener('touchcancel', onTouchEnd, false);
	var newDividerX = e.targetTouches[0].pageX - container.offsetLeft - Math.round((dividerSize /2))
	setDividerPos(container,newDividerX);
	e.preventDefault();
}

function onTouchMove(e){
 	var container = e.targetTouches[0].target.parentNode
	var newDividerX = e.targetTouches[0].pageX - container.offsetLeft - Math.round((dividerSize /2))
	setDividerPos(container,newDividerX);
	e.preventDefault()
}

function onTouchEnd(e) {
	var container = e.target.parentNode
	container.removeEventListener('touchmove', onTouchMove);
	container.removeEventListener('touchend', onTouchEnd);
	container.removeEventListener('touchcancel', onTouchEnd);
	container.onmousemove = null
	container.onmouseup = null
}